#ifndef BAZATESTU_HH
#define BAZATESTU_HH


#include "WyrazenieZesp.hh"

//baza testow
struct BazaTestu {
  WyrazenieZesp  *wskTabTestu;   //wskaznik na tablice z pytaniem
  unsigned int    IloscPytan;    
  unsigned int    IndeksPytania; // numer pytania
};


//inicjalizacja testu
bool InicjalizujTest( BazaTestu  *wskBazaTestu, const char*  sNazwaTestu );

bool PobierzNastpnePytanie( BazaTestu  *wskBazaTestu,  WyrazenieZesp *wskWyr );

#endif
