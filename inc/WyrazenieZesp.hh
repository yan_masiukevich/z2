#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH



#include "LZespolona.hh"


//operatory
enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };



// argumenty
struct WyrazenieZesp {
  LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
  Operator     Op;     // Opertor wyrazenia arytmetycznego
  LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
};


/*
 * Funkcje ponizej są zdefiniowane w module.
 *
 */
void Wyswietl(WyrazenieZesp WyrZ);
LZespolona Oblicz(WyrazenieZesp WyrZ);
WyrazenieZesp Wczytywanie(WyrazenieZesp WyrZ);
#endif
