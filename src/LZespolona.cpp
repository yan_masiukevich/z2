#include "LZespolona.hh"
#include <iostream>
#include <iomanip> 
#include <cmath>

#define PI 3.14159

using namespace std;


// dodawanie 
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}
// odejmowanie
LZespolona  operator - (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  return Wynik;
}
//mnozenie
LZespolona  operator * (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = (Skl1.re*Skl2.re) - (Skl1.im*Skl2.im);
  Wynik.im = (Skl1.re*Skl2.im) + (Skl1.im*Skl2.re);
  return Wynik;
}
//dzielenie

LZespolona operator / (LZespolona Skl1, LZespolona Skl2)
{
    LZespolona Wynik;

    if(Skl2.re==0 && Skl2.im==0){
        cout<<" BLAD "<<endl;
        Wynik.re = Wynik.im = 999999999999999;
    }
    else{
        Wynik.re = (((Skl1.re * Skl2.re) + (Skl1.im * Skl2.im)) / ((Skl2.re * Skl2.re) + (Skl2.im * Skl2.im)));
        Wynik.im = (((Skl1.im * Skl2.re) - (Skl1.re * Skl2.im)) / ((Skl2.re * Skl2.re) + (Skl2.im * Skl2.im)));
    }
    return Wynik;
}


//wczytywanie

LZespolona WczytajL(LZespolona Skl1)
{
    cout << "Podaj część rzeczewistą liczby zespolonej:  ";
    cin >> Skl1.re;
    cout << "Podaj część urojoną liczby zespolonej:  ";
    cin >> Skl1.im;
    cout << endl;

    return Skl1;
}



//wyswietlanie
void Wyswietl(LZespolona Skl1)
{
    cout << "(" << fixed << setprecision(1) << Skl1.re << showpos << Skl1.im << noshowpos << "i)" << setprecision(6);
}



LZespolona operator += (LZespolona Skl1, LZespolona Skl2)
{
  Skl1 = Skl1 + Skl2;
  return Skl1;
}

LZespolona operator *= (LZespolona Skl1, LZespolona Skl2)
{
  Skl1 = Skl1 * Skl2;
  return Skl1;
}

double Arg(LZespolona Z1)
{
  double WynikR;  // wynik w radianach
  double WynikS; //wynik w stopniach
  if (Z1.re !=0)
  {
    if (Z1.re > 0)
    {
     WynikR = atan2(Z1.re, Z1.im);
    }
    else
    {
      WynikR = atan2(Z1.re, Z1.im) + PI;
    }
  }
  else if (Z1.im > 0)
  {
    WynikR = PI/2;
  }
  else if (Z1.im < 0)
  {
    WynikR = -PI/2;
  }

  if (Z1.im != 0 && Z1.re != 0){ 
  
  WynikS = (WynikR * 180) / PI;  //stopnie
  }
  else {
    cout << "Podaj inne dane dla liczby zespolonej";
  }
  
  return WynikS;
}



