#include "WyrazenieZesp.hh"
#include <iostream>
#include <iomanip>

using namespace std;

WyrazenieZesp Wczytywanie(WyrazenieZesp WyrZ) {
    char A;

    cout << "Podaj część rzeczewistą 1 arg:  ";
    cin >> WyrZ.Arg1.re;
    cout << "Podaj część urojoną 1 arg:  ";
    cin >> WyrZ.Arg1.im >> "i";
    cout << endl;
    cout << "Podaj część rzeczewistą 2 arg:  ";
    cin >> WyrZ.Arg2.re;
    cout << "Podaj część urojoną 2 arg:  ";
    cin >> WyrZ.Arg2.im >> "i";
    cout << endl;

     if (A == '+' || A == '-' || A == '*' || A == '/'){                                         
        cout << "Podaj działanie z liczbami(+-*/):  ";
        cin >> A;
        switch (A) {
            case '+':
                WyrZ.Op = Op_Dodaj;
                break;
            case '-':
                WyrZ.Op = Op_Odejmij;
                break;
            case '*':
                WyrZ.Op = Op_Mnoz;
                break;
            case '/':
                WyrZ.Op = Op_Dziel;
                break;
        }
    }else {
      cout << endl;  
    }
    return WyrZ;
}

/*
     WyrZ.Arg1 - pierwsza liczba zespolona,
     WyrZ.Arg2 - druga liczba zespolona,
     Wyrz.Op   - operator arytmetyczny,
     Wyn       - wynik 
 */
LZespolona Oblicz(WyrazenieZesp WyrZ)
{
    LZespolona Wyn;
    if(WyrZ.Op == Op_Dodaj)
    {
        Wyn.re = WyrZ.Arg1.re + WyrZ.Arg2.re;
        Wyn.im = WyrZ.Arg1.im + WyrZ.Arg2.im;
    }
    if(WyrZ.Op == Op_Odejmij)
    {
        Wyn.re = WyrZ.Arg1.re - WyrZ.Arg2.re;
        Wyn.im = WyrZ.Arg1.im - WyrZ.Arg2.im;
    }
    if(WyrZ.Op == Op_Mnoz)
    {
        Wyn.re = (WyrZ.Arg1.re * WyrZ.Arg2.re) - (WyrZ.Arg1.im * WyrZ.Arg2.im);
        Wyn.im = (WyrZ.Arg1.re * WyrZ.Arg2.im) + (WyrZ.Arg1.im * WyrZ.Arg2.re);
    }
    if(WyrZ.Op == Op_Dziel)
    {
        if(WyrZ.Arg2.re==0 && WyrZ.Arg2.im==0){                                            
            cout << " BLAD " << endl;
            Wyn.re = Wyn.im = 999999999999999;
            }
        else{
            Wyn.re = (((WyrZ.Arg1.re * WyrZ.Arg2.re) + (WyrZ.Arg1.im * WyrZ.Arg2.im)) / ((WyrZ.Arg2.re * WyrZ.Arg2.re) + (WyrZ.Arg2.im * WyrZ.Arg2.im)));
            Wyn.im = (((WyrZ.Arg1.im * WyrZ.Arg2.re) - (WyrZ.Arg1.re * WyrZ.Arg2.im)) / ((WyrZ.Arg2.re * WyrZ.Arg2.re) + (WyrZ.Arg2.im * WyrZ.Arg2.im)));
        }
    }
    return Wyn;
}

//wyświetlenie wyrazenia zespolonego

void Wyswietl(WyrazenieZesp WyrZ)
{
    cout << "(" << fixed << setprecision(1) << WyrZ.Arg1.re << showpos << WyrZ.Arg1.im << noshowpos << "i)" << setprecision(6);
    switch(WyrZ.Op)
    {
        case Op_Dodaj: cout << " + "; break;
        case Op_Odejmij: cout << " - "; break;
        case Op_Mnoz: cout << " * "; break;
        case Op_Dziel: cout << " / "; break;
    }
    cout << "(" << fixed << setprecision(1) << WyrZ.Arg2.re << showpos << WyrZ.Arg2.im << noshowpos << "i)" << setprecision(6);
}