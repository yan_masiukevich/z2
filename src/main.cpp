#include <iostream>
#include "BazaTestu.hh"
#include <iomanip>

using namespace std;


std::ostream &operator<<(std::ostream &out, const LZespolona &Skl1);
std::istream &operator>>(std::istream &is, LZespolona &Skl1);


int main(int argc, char **argv) {

    if (argc < 2) {
        cout << " Brak opcji okreslajacej rodzaj testu." << endl;
        cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
        return 1;
    }


    BazaTestu BazaT = {nullptr, 0, 0};

    if (InicjalizujTest(&BazaT, argv[1]) == false) {
        cerr << " nieposzła" << endl;
        return 1;
    }


    cout << endl;
    cout << " Start testu : " << argv[1]  << endl;
    cout << endl;

    WyrazenieZesp WyrZ_PytanieTestowe;

    while (PobierzNastpnePytanie(&BazaT, &WyrZ_PytanieTestowe)) {
        cout << " Czesc rzeczywista pierwszego argumentu: ";
        cout << WyrZ_PytanieTestowe.Arg1.re << endl;
    }


    

    cout << "Test wczytywania <<" << endl << endl;

    LZespolona Wyn, Skl3, Skl4;                               
    cout << "Wczytanie za pomocą funkcji" << endl;
    Skl3 = WczytajL(Skl3);
    cout << "Wczytanie za pomocą przeciążenia " << endl;
    cin >> Skl4;
// wyswietlenia 

    cout << "Test obliczania" << endl;

    WyrazenieZesp WyrZ;                                       //! Deklaracja zmiennej typu WyrazenieZesp

    WyrZ = Wczytywanie(WyrZ);                                   //! Wczytanie wyrazenia zespolonego
    cout << endl << "Wyrażenie:    ";
    Wyswietl(WyrZ);                                           //! Wyswietlenie wyrazenia zespolonego
    cout << " = ";
    Wyn = Oblicz(WyrZ);                                       //! Obliczenie wyniku wyrazenia zespolonego
    cout << Wyn;

    

} 
//Realizuje wyswietlenie liczby zespolej na standardowe wyjscie za pomocą przeciazenia operatora
std::ostream &operator<<(std::ostream &out, const LZespolona &Skl1)
{
    out << "(" << fixed << setprecision(1) << Skl1.re << showpos << Skl1.im << noshowpos << "i)" << setprecision(6);
    return out;
}
//Realizuje wczytanie liczby zespolej za pomocą przeciążenia 
std::istream &operator>>(std::istream &is, LZespolona &Skl1) {
    cout << endl << "Podaj czesc rzeczywistą:  ";
    is >> Skl1.re;
    cout << "Podaj czesc urojoną  :";
    is >> Skl1.im;
    cout << endl;
    return is;
}
